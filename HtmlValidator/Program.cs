﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Resources;
using System.Collections;
using log4net;

public class Program
{
    private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    [STAThread]
    public static void Main(string[] args)
    {
        Console.Clear();

        var folderBrowser = new FolderBrowserDialog();
        var folderName = string.Empty;

        if (folderBrowser.ShowDialog() == DialogResult.OK)
        {
            folderName = folderBrowser.SelectedPath;
        }

        var dirs = Directory.GetFiles(folderName, "*.resx");

        foreach (var file in dirs)
        {
            var entries = new ResXResourceReader(file);
            foreach (DictionaryEntry entry in entries)
            {
                var fileName = new FileInfo(file).Name;

                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(entry.Value.ToString());

                if (doc.ParseErrors.Count() > 0)
                {
                    foreach (var error in doc.ParseErrors)
                    {
                        Logger.Error(
                            $"File Path: { file } , File Name: { fileName }, key : { entry.Key }, Value: { entry.Value }, Error: { error.Reason }"
                        );
                    }
                }
            }
        }
    }
}